package games

import (
	apigame "bisoncasino.net/api/games"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().Unix())
}

type CardSlice []apigame.Card

func (cardSlice CardSlice) Len() int {
	return len(cardSlice)
}

func (cardSlice CardSlice) Less(i, j int) bool {
	return cardSlice[i].Number < cardSlice[j].Number
}

func (cardSlice CardSlice) Swap(i, j int) {
	cardSlice[i], cardSlice[j] = cardSlice[j], cardSlice[i]
}

func (cardSlice CardSlice) GetSlice() []apigame.Card {
	return cardSlice[:]
}

type Deck struct {
	cards []apigame.Card
}

func NewDeck() (deck Deck) {
	deck = Deck{make([]apigame.Card, 52)}
	var card apigame.Card
	for i := 0; i < 4; i++ {
		switch i {
		case 0:
			card.Suit = apigame.Spades
		case 1:
			card.Suit = apigame.Hearts
		case 2:
			card.Suit = apigame.Diamonds
		case 3:
			card.Suit = apigame.Clubs
		}
		for j := 0; j < 13; j++ {
			card.Number = j + 1
			deck.cards[i*13+j] = card
		}
	}
	return
}

func (d *Deck) Shuffle() {
	rand.Shuffle(len(d.cards), func(i, j int) {
		d.cards[i], d.cards[j] = d.cards[j], d.cards[i]
	})
}

func (d *Deck) GetCards(n int) (card []apigame.Card) {
	for i := 0; i < n; i++ {
		card = append(card, d.cards[i])
	}
	d.cards = d.cards[n:]
	return
}
