package poker

import (
	"fmt"
)

type TableManager struct {
	tables       map[string]*Table
	lastNumberID int
}

func NewTableManager() *TableManager {
	return &TableManager{tables: make(map[string]*Table), lastNumberID: 1000}
}

func (t *TableManager) NewTable(bb int) string {
	t.lastNumberID++
	id := fmt.Sprintf("T%d", t.lastNumberID)
	t.tables[id] = NewTable(bb)
	return id
}

func (t *TableManager) GetTable(id string) *Table {
	if tb, ok := t.tables[id]; ok {
		return tb
	}
	return nil
}
