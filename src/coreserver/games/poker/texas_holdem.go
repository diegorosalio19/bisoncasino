package poker

import (
	apigames "bisoncasino.net/api/games"
	apipoker "bisoncasino.net/api/games/poker"
	"bisoncasino.net/coreserver/games"
	"bisoncasino.net/coreserver/games/table"
	"errors"
	"strconv"
	"time"
)

type Table struct {
	*table.Table
	bigBlind int
	ctx      *gameContext
}

func NewTable(bb int) *Table {
	t := &Table{
		bigBlind: bb,
		ctx:      nil,
	}
	t.Table = table.NewTable(t)
	return t
}

func (t *Table) PlayGame() error {
	defer func() {
		t.PlayersGroup.SendBroadcastNews(apipoker.GameEnded())
		if e := recover(); e != nil {
			t.Logger.Println("game got panic with error:", e)
			if us, err := t.ctx.players.Get(); err == nil {
				p, err := t.GetPlayer(us)
				if err != nil {
					p.Balance += t.ctx.pot
				}
				t.PlayersGroup.SendBroadcastNews(apipoker.PlayerBalanceChanged(us, p.Balance))
				return
			}
		}
	}()
	if t.Players.Len() < 2 {
		return errors.New("not enough player " + strconv.Itoa(t.Players.Len()))
	}

	t.ctx = newRoundContext()
	t.PlayersGroup.SendBroadcastNews(apipoker.GameStarted())
	t.Players.Next()
	// Pre-flop
	for i := 0; i < t.Players.Len(); i++ {
		str, _ := t.Players.Get()
		p, _ := t.GetPlayer(str)
		if p.Balance >= t.bigBlind {
			t.ctx.players.InsertBefore(str)
		}
		t.Players.Next()
	}

	playersPhaseBet := make(map[string]int)

	payTax := func(psm *table.Player, amount int) {
		t.playerPay(psm, amount)
		playersPhaseBet[psm.Username] = amount
		t.PlayersGroup.SendBroadcastNews(apipoker.PlayerPlayed(psm.Username, apipoker.Check, amount))
	}

	p := t.nextPlayer()
	if p == nil {
		return errors.New("not enough player")
	}
	payTax(p, t.bigBlind/2)

	p = t.nextPlayer()
	if p == nil {
		return errors.New("not enough player")
	}
	payTax(p, t.bigBlind)

	t.ctx.currentBet = t.bigBlind
	t.dealCardsToPlayer()

	t.phase(playersPhaseBet)

	phaseWrapper := func(nCards int) {
		t.ctx.currentBet = 0
		t.ctx.commonCards = append(t.ctx.commonCards, t.ctx.deck.GetCards(nCards)...)
		t.PlayersGroup.SendBroadcastNews(apipoker.CardsObtained(t.ctx.commonCards, true))
		t.phase(nil)
	}
	// Flop
	phaseWrapper(3)
	// Turn
	phaseWrapper(1)
	// River
	phaseWrapper(1)

	// End
	combinations := struct {
		usernames []string
		comb      apigames.Combination
	}{
		usernames: make([]string, 0),
	}

	for username, cards := range t.ctx.playersCards {
		cardsToEvaluate := append(cards, t.ctx.commonCards...)
		comb := games.CheckHandCombinations(cardsToEvaluate)
		result := comb.Compare(combinations.comb)
		switch result {
		case 1:
			combinations.usernames = combinations.usernames[:0]
			combinations.usernames = append(combinations.usernames, username)
			combinations.comb = comb
		case 0:
			combinations.usernames = append(combinations.usernames, username)
		}
	}

	var wn = len(combinations.usernames)
	if wn == 0 {
		wn = 1
	}
	win := t.ctx.pot / wn
	winners := make(map[string]int)
	for _, username := range combinations.usernames {
		winners[username] = win
	}

	t.PlayersGroup.SendBroadcastNews(apipoker.CardsShowed(t.ctx.playersCards))
	t.PlayersGroup.SendBroadcastNews(apipoker.Outcome(combinations.comb, winners))

	for username, w := range winners {
		player, err := t.GetPlayer(username)
		if err != nil {
			continue
		}
		player.Balance += w
		t.PlayersGroup.SendBroadcastNews(apipoker.PlayerBalanceChanged(username, p.Balance))
	}

	<-time.NewTimer(time.Second * 5).C

	return nil
}

func (t *Table) phase(playersPhaseBet map[string]int) {
	if t.ctx.players.Len() < 2 {
		return
	}
	if playersPhaseBet == nil {
		playersPhaseBet = make(map[string]int)
	}
	for i := 0; i < t.ctx.players.Len(); i++ {
		p := t.nextPlayer()
		if p == nil {
			continue
		}
		alreadyPaid := playersPhaseBet[p.Username]
		t.PlayersGroup.SendBroadcastNews(apipoker.PlayerPlaying(p.Username))
		resp, err := p.PlayFunc(apipoker.PlayRequest(t.ctx.currentBet, alreadyPaid))
		if err != nil {
			t.playerFold(p)
			continue
		}
		r := apipoker.PlayResponse(resp)
		switch r.PlayerAction {
		case apipoker.Fold:
			t.playerFold(p)
		case apipoker.Recall:
			t.ctx.currentBet = r.RecallValue
			i = 0
			fallthrough
		case apipoker.Call:
			t.playerPay(p, t.ctx.currentBet-alreadyPaid)
			playersPhaseBet[p.Username] = t.ctx.currentBet
		case apipoker.Check:
		}

		t.PlayersGroup.SendBroadcastNews(apipoker.PlayerPlayed(p.Username, r.PlayerAction, playersPhaseBet[p.Username]))
	}
	tot := 0
	for _, v := range playersPhaseBet {
		tot += v
	}
	t.addToPot(tot)
}

func (t *Table) nextPlayer() *table.Player {
	t.ctx.players.Next()
	for {
		str, err := t.ctx.players.Get()
		if err != nil {
			return nil
		}
		p, err := t.GetPlayer(str)
		if err != nil {
			t.ctx.players.Remove()
			if t.ctx.players.Len() < 2 {
				panic("not enough player")
			}
		} else {
			return p
		}
	}
}

func (t *Table) dealCardsToPlayer() {
	for i := 0; i < t.ctx.players.Len(); i++ {
		p := t.nextPlayer()
		if p == nil {
			continue
		}
		cards := t.ctx.deck.GetCards(2)
		t.ctx.playersCards[p.Username] = cards
		p.NewsFunc(apipoker.CardsObtained(cards, false))
	}
}

func (t *Table) playerPay(p *table.Player, amount int) {
	p.Balance -= amount
	t.PlayersGroup.SendBroadcastNews(apipoker.PlayerBalanceChanged(p.Username, p.Balance))
}

func (t *Table) addToPot(amount int) {
	t.ctx.pot += amount
	t.PlayersGroup.SendBroadcastNews(apipoker.PotChanged(t.ctx.pot))
}

func (t *Table) playerFold(p *table.Player) {
	delete(t.ctx.playersCards, p.Username)
	t.ctx.players.SearchAndRemove(p.Username)
}

type gameContext struct {
	deck            games.Deck
	players         *table.StringRing
	playersCards    map[string][]apigames.Card
	commonCards     []apigames.Card
	pot, currentBet int
}

func newRoundContext() *gameContext {
	rc := &gameContext{
		deck:         games.NewDeck(),
		players:      new(table.StringRing),
		playersCards: make(map[string][]apigames.Card),
		commonCards:  make([]apigames.Card, 0, 5),
		pot:          0,
		currentBet:   0,
	}
	rc.deck.Shuffle()
	return rc
}
