package poker

import (
	"bisoncasino.net/api"
	apinews "bisoncasino.net/api/news"
	"bisoncasino.net/coreserver/games/table"
	"bisoncasino.net/coreserver/users"
	"encoding/json"
	"errors"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strconv"
)

func GetAPIHandler(u users.UserManager, tm *TableManager) func(writer http.ResponseWriter, request *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		// auth in websocket, alternative https://stackoverflow.com/questions/4361173/http-headers-in-websockets-client-api
		api.InitJSONPublicResponse(writer)
		var fields = api.ParseField(request.URL.Path, "/poker_texas_holdem/")
		switch len(fields) {
		case 0:
			if request.Method == http.MethodGet {
				servicePokerTexasHoldem(u, tm, writer, request)
			}
			if request.Method == http.MethodPost {
				serviceCreatePokerTable(tm, writer, request)
			}
		}
	}
}

func serviceCreatePokerTable(tm *TableManager, writer http.ResponseWriter, request *http.Request) {
	pars := request.URL.Query().Get("big_blind")
	bb := 50
	if pars != "" {
		bb, _ = strconv.Atoi(pars)
	}
	id := tm.NewTable(bb)
	writer.WriteHeader(http.StatusCreated)
	_ = json.NewEncoder(writer).Encode(struct {
		ID string
	}{id})
}

func servicePokerTexasHoldem(u users.UserManager, tm *TableManager, writer http.ResponseWriter, request *http.Request) {
	parameters := request.URL.Query()
	var tab *Table
	if !parameters.Has("table_id") || !parameters.Has("session_balance") {
		writer.WriteHeader(http.StatusNotAcceptable)
		return
	} else {
		tab = tm.GetTable(parameters.Get("table_id"))
		if tab == nil {
			writer.WriteHeader(http.StatusNotAcceptable)
			return
		}
	}
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	conn, err := upgrader.Upgrade(writer, request, nil)
	if err != nil {
		return
	}
	_, message, err := conn.ReadMessage()
	if err != nil {
		log.Println(err)
		_ = conn.Close()
		return
	}
	user, err := u.GetUser(string(message))
	if err != nil {
		log.Println(err)
		_ = conn.Close()
		return
	}

	bal, _ := strconv.Atoi(parameters.Get("session_balance"))
	user.Balance -= float32(bal)
	player := table.NewPlayer(user.Username, bal, nil, nil)

	readCh := make(chan []byte)
	go func() {
		for {
			_, rn, err := conn.ReadMessage()
			if err != nil {
				close(readCh)
				return
			}
			readCh <- rn
		}
	}()

	player.PlayFunc = func(news apinews.News) (apinews.News, error) {
		_ = conn.WriteMessage(websocket.TextMessage, news)
		if n, ok := <-readCh; ok {
			return n, nil
		}
		return nil, errors.New("socket closed")
	}
	player.NewsFunc = func(news apinews.News) {
		_ = conn.WriteMessage(websocket.TextMessage, news)
	}

	conn.SetCloseHandler(func(code int, text string) error {
		_ = tab.Quit(player)
		return nil
	})
	err = tab.Join(player)
	if err != nil {
		log.Println(err)
		return
	}
	<-player.ReadyToQuit
	user.Balance += float32(player.Balance)
}
