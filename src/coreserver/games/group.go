package games

import (
	apinews "bisoncasino.net/api/news"
	"errors"
	"log"
	"os"
	"sync"
)

type Member interface {
	GetUsername() string
	OnNews(news apinews.News)
}

type Group struct {
	members sync.Map
	logger  log.Logger
}

func NewGroup(id string) *Group {
	g := new(Group)

	g.logger.SetPrefix("[Group#" + id + "] ")
	g.logger.SetFlags(log.Lshortfile)
	g.logger.SetOutput(os.Stdout)

	return g
}

func (g *Group) Join(m Member) error {
	_, found := g.members.Load(m.GetUsername())
	if found {
		return errors.New("user already exists")
	}
	g.SendBroadcastNews(apinews.MemberJoin(m.GetUsername()))
	g.logger.Println(m.GetUsername(), "join")
	g.members.Store(m.GetUsername(), m)
	return nil
}

func (g *Group) Quit(m Member) error {
	_, found := g.members.Load(m.GetUsername())
	if !found {
		return errors.New("user doesn't exist")
	}
	g.members.Delete(m.GetUsername())
	g.SendBroadcastNews(apinews.MemberQuit(m.GetUsername()))
	g.logger.Println(m.GetUsername(), "quit")
	return nil
}

func (g *Group) GetMember(username string) (member Member, err error) {
	if m, found := g.members.Load(username); !found {
		err = errors.New("user doesn't exist")
	} else {
		member = m.(Member)
	}
	return
}

func (g *Group) SendBroadcastNews(news apinews.News) {
	g.members.Range(func(_, value any) bool {
		m := value.(Member)
		m.OnNews(news)
		return true
	})
}
