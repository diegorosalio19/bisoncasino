package slot

import (
	"bisoncasino.net/api"
	apigame "bisoncasino.net/api/games"
	"bisoncasino.net/coreserver/users"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"os"
)

func GetAPIHandler(u users.UserManager) func(w http.ResponseWriter, r *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		// auth in websocket, alternative https://stackoverflow.com/questions/4361173/http-headers-in-websockets-client-api
		api.InitJSONPublicResponse(writer)
		var fields = api.ParseField(request.URL.Path, "/slot_machine/")
		switch len(fields) {
		case 0:
			if request.Method == http.MethodGet {
				servicePokerSlot(u, writer, request)
			}
		}
	}
}

func servicePokerSlot(u users.UserManager, writer http.ResponseWriter, request *http.Request) {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
	conn, err := upgrader.Upgrade(writer, request, nil)
	if err != nil {
		return
	}
	_, message, err := conn.ReadMessage()
	if err != nil {
		log.Println(err)
		_ = conn.Close()
		return
	}
	user, err := u.GetUser(string(message))
	if err != nil {
		log.Println(err)
		_ = conn.Close()
		return
	}
	var closed = false
	conn.SetCloseHandler(func(code int, text string) error {
		closed = true
		return nil
	})
	for !closed {
		var slotRequest apigame.PlayPokerSlotRequest
		err = conn.ReadJSON(&slotRequest)

		if err != nil {
			log.Println(err)
			continue
		}
		if user.Balance-slotRequest.Bet >= 0 {
			user.Balance -= slotRequest.Bet
			hand := ConvertHandToAPIResponse(GetHand(slotRequest.Bet))
			user.Balance += hand.Value
			log.New(os.Stdout, user.Username, 0).Println(hand)
			_ = conn.WriteJSON(hand)
		} else {
			_ = conn.WriteMessage(websocket.TextMessage, []byte("error"))
		}

	}
}
