package slot

import (
	apigames "bisoncasino.net/api/games"
	"bisoncasino.net/coreserver/games"
	"sort"
)

var CombinationPayments = map[apigames.CombinationMajor]float32{
	apigames.HighCard:      0.0,
	apigames.Pair:          1 / 0.67,     //0.58,
	apigames.TwoPair:       1 / 0.28,     //0.11,
	apigames.ThreeOfAKind:  1 / 0.076,    //0.023,
	apigames.Straight:      1 / 0.014,    //0.0039
	apigames.Flush:         1 / 0.0068,   //0.0019
	apigames.FullHouse:     1 / 0.0049,   //0.0014
	apigames.FourOfAKind:   1 / 0.00071,  //0.0002
	apigames.StraightFlush: 1 / 0.000054} //0.000015

type Hand struct {
	Cards       games.CardSlice
	Combination apigames.CombinationMajor
	Value       float32
}

var deck games.Deck

func init() {
	deck = games.NewDeck()
}

func GetHand(val float32) (hand Hand) {
	deck = games.NewDeck()
	deck.Shuffle()
	for i := 0; i < 5; i++ {
		hand.Cards = append(hand.Cards, deck.GetCards(1)...)
	}
	comb := games.CheckHandCombinations(hand.Cards).CombinationMajor
	sort.Sort(hand.Cards)
	hand.Combination = comb
	hand.Value = val * CombinationPayments[comb]
	return
}

func ConvertHandToAPIResponse(hand Hand) apigames.PlayPokerSlotResponse {
	cs := hand.Cards.GetSlice()
	var cs0 = make([]apigames.Card, 0)
	for _, c := range cs {
		cs0 = append(cs0, apigames.Card{
			Number: c.Number,
			Suit:   c.Suit,
		})
	}
	return apigames.PlayPokerSlotResponse{
		Cards:       cs0,
		Combination: string(hand.Combination),
		Value:       hand.Value,
	}
}
