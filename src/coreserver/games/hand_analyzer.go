package games

import (
	apigames "bisoncasino.net/api/games"
	"sort"
)

func CheckHandCombinations(cards CardSlice) apigames.Combination {
	cs := make(CardSlice, len(cards))
	copy(cs, cards)
	sort.Sort(cs)
	straight, sp := isStraight(cs)
	flush, fp := isFlush(cs)
	if straight && flush {
		return apigames.Combination{CombinationMajor: apigames.StraightFlush, Score: sp}
	} else if !straight && flush {
		return apigames.Combination{CombinationMajor: apigames.Flush, Score: fp}
	} else if straight && !flush {
		return apigames.Combination{CombinationMajor: apigames.Straight, Score: sp}
	}
	synthesizedHand := handSynthesizer(cs)
	if ok, value := isPoker(synthesizedHand); ok {
		return apigames.Combination{CombinationMajor: apigames.FourOfAKind, Score: value}
	}
	if synthesizedHand.Len() >= 2 {
		score := synthesizedHand[0].number
		score = score << 4
		score += synthesizedHand[1].number
		if synthesizedHand[0].quantity == 3 && synthesizedHand[1].quantity == 2 {
			return apigames.Combination{CombinationMajor: apigames.FullHouse, Score: score}
		} else if synthesizedHand[0].quantity == 2 && synthesizedHand[1].quantity == 2 {
			return apigames.Combination{CombinationMajor: apigames.TwoPair, Score: score}
		}
	} else if synthesizedHand.Len() >= 1 {
		score := synthesizedHand[0].number
		switch synthesizedHand[0].quantity {
		case 3:
			return apigames.Combination{CombinationMajor: apigames.ThreeOfAKind, Score: score}
		case 2:
			return apigames.Combination{CombinationMajor: apigames.Pair, Score: score}
		}
	}
	return apigames.Combination{CombinationMajor: apigames.HighCard, Score: cs[0].Number}
}

type syntheticInfo struct {
	number   int
	quantity int
}

type syntheticInfoSlice []syntheticInfo

func (r syntheticInfoSlice) Less(i, j int) bool {
	if r[i].quantity == r[j].quantity {
		var a, b = r[i].number, r[j].number
		if a == 1 {
			a = 14
		}
		if b == 1 {
			b = 14
		}
		return a < b
	}
	return r[i].quantity < r[j].quantity
}

func (r syntheticInfoSlice) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

func (r syntheticInfoSlice) Len() int {
	return len(r)
}

func handSynthesizer(cards CardSlice) (handComb syntheticInfoSlice) {
	var handMap = make(map[int]int)
	for _, card := range cards {
		handMap[card.Number]++
	}
	for k, v := range handMap {
		if v != 1 {
			handComb = append(handComb, syntheticInfo{
				number:   k,
				quantity: v,
			})
		}
	}
	sort.Sort(handComb)
	return
}

func isPoker(synthesizedHand syntheticInfoSlice) (bool, int) {
	if len(synthesizedHand) == 0 {
		return false, 0
	} else if synthesizedHand[0].quantity == 4 {
		return true, synthesizedHand[0].number
	}
	return false, 0
}

func isFlush(cards CardSlice) (bool, int) {
	var suitMap = map[apigames.Suit]int{}
	var suitBest = map[apigames.Suit]int{}
	for _, card := range cards {
		suitMap[card.Suit]++
		suitBest[card.Suit] = card.Number
	}
	for k, v := range suitMap {
		if v == 5 {
			return true, suitBest[k]
		}
	}
	return false, 0
}

func isStraight(cs CardSlice) (bool, int) {
	if len(cs) < 5 {
		return false, 0
	}
	if cs[0].Number == 1 {
		cs = append(cs, apigames.Card{Number: 14})
	}
	for i := 0; i < len(cs)-4; i++ {
		if (cs[i+0].Number == cs[i+1].Number-1) &&
			(cs[i+1].Number == cs[i+2].Number-1) &&
			(cs[i+2].Number == cs[i+3].Number-1) &&
			(cs[i+3].Number == cs[i+4].Number-1) {
			return true, cs[i+0].Number
		}
	}
	return false, 0
}
