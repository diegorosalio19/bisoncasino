package table

import "testing"

func TestStringRing_InsertBefore(t *testing.T) {
	el := []string{"1", "2", "3", "4"}
	s := new(StringRing)
	s.InsertBefore(el[0])
	s.InsertBefore(el[1])
	s.InsertBefore(el[2])
	s.InsertBefore(el[3])

	for i := 0; i < s.Len(); i++ {
		get, _ := s.Get()
		t.Log(get)
		if get != el[i] {
			t.FailNow()
		}
		s.Next()
	}
}

func TestStringRing_SearchAndRemove(t *testing.T) {
	el := []string{"1", "2", "3", "4"}
	s := new(StringRing)
	s.InsertBefore(el[1])
	s.InsertBefore(el[0])
	s.SearchAndRemove(el[1])

	for i := 0; i < s.Len(); i++ {
		get, _ := s.Get()
		t.Log(get)
		s.Next()
	}
}
