package table

import (
	apipoker "bisoncasino.net/api/games/poker"
	apinews "bisoncasino.net/api/news"
	"bisoncasino.net/coreserver/games"
	"container/ring"
	"errors"
	"log"
	"os"
	"strconv"
	"sync"
)

var tid = 0

type PlayableTable interface {
	PlayGame() error
}

type StringRing struct {
	ring *ring.Ring
	mx   sync.RWMutex
}

func (s *StringRing) Len() int {
	s.mx.RLock()
	defer s.mx.RUnlock()
	return s.ring.Len()
}

func (s *StringRing) Next() {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.ring == nil {
		return
	}
	s.ring = s.ring.Next()
}

func (s *StringRing) Prev() {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.ring == nil {
		return
	}
	s.ring = s.ring.Prev()
}

func (s *StringRing) Get() (string, error) {
	s.mx.RLock()
	defer s.mx.RUnlock()
	if s.ring == nil {
		return "", errors.New("empty ring")
	}
	return s.ring.Value.(string), nil
}

func (s *StringRing) InsertBefore(ss string) {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.ring == nil {
		s.ring = ring.New(1)
		s.ring.Value = ss
	} else {
		r := ring.New(1)
		r.Value = ss
		s.ring.Prev().Link(r)
	}
}

func (s *StringRing) Remove() {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.ring.Len() == 1 {
		s.ring = nil
		return
	}
	s.ring = s.ring.Prev()
	s.ring.Unlink(1)
	s.ring = s.ring.Next()
}

func (s *StringRing) SearchAndRemove(ss string) {
	s.mx.Lock()
	defer s.mx.Unlock()
	if s.ring == nil {
		return
	}
	if s.ring.Len() == 1 && s.ring.Value.(string) == ss {
		s.ring = nil
		return
	}
	for i := 0; i < s.ring.Len(); i++ {
		if s.ring.Value.(string) == ss {
			s.ring = s.ring.Prev()
			s.ring.Unlink(1)
		}
		s.ring = s.ring.Next()
	}
}

type Table struct {
	PlayersGroup *games.Group
	Players      *StringRing

	PlayableTable PlayableTable
	Logger        log.Logger
}

func NewTable(pt PlayableTable) *Table {
	tid++
	stid := strconv.Itoa(tid)

	t := new(Table)
	t.PlayersGroup = games.NewGroup(stid)
	t.PlayableTable = pt

	t.Players = new(StringRing)

	t.Logger.SetPrefix("[Table#" + stid + "] ")
	t.Logger.SetFlags(log.Lshortfile)
	t.Logger.SetOutput(os.Stdout)

	return t
}

func (t *Table) SendPlayerList() {
	players := make([]apipoker.Player, 0, t.Players.Len())
	for i := 0; i < t.Players.Len(); i++ {
		if username, err := t.Players.Get(); err == nil {
			member, err := t.PlayersGroup.GetMember(username)
			if err == nil {
				players = append(players, apipoker.Player{
					Username: member.(*Player).Username,
					Balance:  member.(*Player).Balance,
				})
			}
		}
		t.Players.Next()
	}
	t.PlayersGroup.SendBroadcastNews(apipoker.PlayerList(players))
}

func (t *Table) Join(p *Player) error {
	if err := t.PlayersGroup.Join(p); err != nil {
		return err
	}
	t.Players.InsertBefore(p.Username)
	t.SendPlayerList()
	if t.Players.Len() == 2 {
		t.Start()
	}
	return nil
}

func (t *Table) Quit(p *Player) error {
	t.Players.SearchAndRemove(p.Username)
	if err := t.PlayersGroup.Quit(p); err != nil {
		return err
	}
	t.SendPlayerList()
	go func() {
		p.ReadyToQuit <- 1
	}()
	return nil
}

func (t *Table) GetPlayer(username string) (*Player, error) {
	member, err := t.PlayersGroup.GetMember(username)
	if err != nil {
		return nil, err
	}
	return member.(*Player), nil
}

func (t *Table) Start() {
	t.Logger.Println("starting...")
	go t.loop()
}

func (t *Table) loop() {
	for {
		t.Logger.Println("new game started...")
		if err := t.PlayableTable.PlayGame(); err != nil {
			t.Logger.Println("game ended with error:", err)
			return
		}
		t.Logger.Println("game ended...")
	}
}

type PlayCallback func(news apinews.News) (apinews.News, error)
type NewsCallback func(news apinews.News)

type Player struct {
	Username string
	Balance  int

	PlayFunc PlayCallback
	NewsFunc NewsCallback

	ReadyToQuit chan any
}

func NewPlayer(un string, bal int, playCallback PlayCallback, newsCallback NewsCallback) *Player {
	return &Player{
		Username:    un,
		Balance:     bal,
		PlayFunc:    playCallback,
		NewsFunc:    newsCallback,
		ReadyToQuit: make(chan any),
	}
}

func (p *Player) GetUsername() string {
	return p.Username
}

func (p *Player) OnNews(n apinews.News) {
	p.NewsFunc(n)
}
