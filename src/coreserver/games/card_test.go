package games

import (
	"testing"
)

func TestDeck_GetCards(t *testing.T) {
	d := NewDeck()
	d.Shuffle()
	l1 := len(d.cards)
	d.GetCards(2)
	l2 := len(d.cards)
	if l1 == l2 {
		t.Errorf("%d, %d", l1, l2)
		t.Fail()
	}
}
