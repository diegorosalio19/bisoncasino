module bisoncasino.net/coreserver

go 1.19

replace bisoncasino.net/api => ../api

require (
	bisoncasino.net/api v0.0.0-00010101000000-000000000000
	github.com/google/uuid v1.3.0
)

require github.com/gorilla/websocket v1.5.0 // indirect
