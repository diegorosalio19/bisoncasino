package main

import (
	"bisoncasino.net/coreserver/games/poker"
	"bisoncasino.net/coreserver/games/slot"
	"bisoncasino.net/coreserver/users"
	"encoding/json"
	"log"
	"net/http"
	"os"
)

type Setting struct {
	Production struct {
		Domain     string `json:"domain"`
		InternalIP string `json:"internalIP"`
	} `json:"production"`
	Development struct {
		Domain     string `json:"domain"`
		InternalIP string `json:"internalIP"`
	} `json:"development"`
	Lan struct {
		InternalIP string `json:"internalIP"`
	} `json:"lan"`
	Mode string `json:"mode"`
}

func main() {
	var usersManager users.UserManager
	usersManager = users.NewFileUserManager("data.json")

	var tablesManager = poker.NewTableManager()

	http.HandleFunc("/api/v1/user/", users.GetAPIHandler(usersManager))
	http.HandleFunc("/api/v1/game/slot_machine/", slot.GetAPIHandler(usersManager))
	http.HandleFunc("/api/v1/game/poker_texas_holdem/", poker.GetAPIHandler(usersManager, tablesManager))
	http.Handle("/", http.FileServer(http.Dir("./site/")))

	var sett Setting
	file, err := os.Open("settings.json")
	if err != nil {
		return
	}
	_ = json.NewDecoder(file).Decode(&sett)
	var host string
	if sett.Mode == "development" {
		host = sett.Development.InternalIP
	} else if sett.Mode == "production" {
		host = sett.Production.InternalIP
	} else {
		host = sett.Lan.InternalIP
	}
	err = http.ListenAndServe(host+":80", nil)
	if err != nil {
		log.Printf("Error: %s", err.Error())
		return
	}
}
