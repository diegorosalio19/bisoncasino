import {
	BackgroundContainerWidget,
	BaseWidget,
	Dimension, FlowLayoutManager,
	FreeLayoutManager,
	ImageWidget,
	MoneyWidget, Position,
	RoundedRectangleWidget
} from './bison-canvas.js'
import {Card, CardWidget, PlayerInfoWidget} from './bison-games.js'

export class PotWidget extends RoundedRectangleWidget {
	private valueWidget: MoneyWidget

	constructor(dimension: Dimension, fontSize: number) {
		super(dimension, 'rgba(0,0,0)', dimension.height / 3)
		this.valueWidget = new MoneyWidget(dimension, fontSize, 'rgba(255,255,255)', 0)
	}

	set value(value: number) {
		this.valueWidget.value = value
	}

	draw(ctx: CanvasRenderingContext2D) {
		super.draw(ctx)
		this.valueWidget.draw(ctx)
	}
}

type ActionResource = { icon: HTMLImageElement, bgColor: string, fgColor: string }
type Action = 'check' | 'fold' | 'call' | 'recall'
const actionResources: Map<Action, ActionResource> = new Map<Action, ActionResource>()
const background: HTMLImageElement = new Image()
const loadIcons = () => {
	const root = document.body.getAttribute('data-bp-root')
	const check = new Image()
	check.src = root + 'bp-res/icons/check-circle-fill.svg'
	const call = check
	const recall = new Image()
	recall.src = root + 'bp-res/icons/arrow-up-circle-fill.svg'
	const fold = new Image()
	fold.src = root + 'bp-res/icons/x-circle-fill.svg'
	actionResources.set('fold', {bgColor: '#dc3545', fgColor: '#ffffff', icon: fold})
	actionResources.set('call', {bgColor: '#0dcaf0', fgColor: '#000000', icon: call})
	actionResources.set('check', {bgColor: '#198754', fgColor: '#ffffff', icon: check})
	actionResources.set('recall', {bgColor: '#ffc107', fgColor: '#000000', icon: recall})
	background.src = root + 'bp-res/background.png'
}
loadIcons()

export class PlayerActionWidget extends RoundedRectangleWidget {
	private layout: FreeLayoutManager
	private readonly iconWidget: ImageWidget
	private readonly valueWidget: MoneyWidget

	constructor(dimension: Dimension, action: Action = 'check') {
		super(dimension, 'rgba(0,0,0)', dimension.height / 3)
		const iconDim = dimension.height / 2
		const margin = (dimension.height - iconDim) / 2
		this.layout = new FreeLayoutManager(dimension, {XRelativeTo: 'left'})
		this.iconWidget = new ImageWidget({width: iconDim, height: iconDim}, actionResources.get(action)!.icon)
		this.valueWidget = new MoneyWidget(
			{width: dimension.width - iconDim - margin * 3, height: dimension.height}, dimension.height / 3,
			'rgba(255,255,255)'
		)
		this.layout.addWidget(this.iconWidget, {x: margin, y: dimension.height / 2})
		this.layout.addWidget(this.valueWidget, {x: margin * 2 + iconDim, y: dimension.height / 2})
	}

	set action(action: Action) {
		super.color = actionResources.get(action)!.bgColor
		this.iconWidget.image = actionResources.get(action)!.icon
		this.valueWidget.color = actionResources.get(action)!.fgColor
	}

	set value(value: number) {
		this.valueWidget.value = value
	}

	draw(ctx: CanvasRenderingContext2D) {
		super.draw(ctx)
		this.layout.draw(ctx)
	}
}

export class PlayerWidget extends BaseWidget {
	private readonly mainLayout: FlowLayoutManager
	private readonly infoLayout: FlowLayoutManager

	private readonly playerInfo: { widget: PlayerInfoWidget, visible: boolean }
	private readonly playerCards: { widget: FlowLayoutManager, visible: boolean }
	private readonly playerAction: { widget: PlayerActionWidget, visible: boolean }

	cardsScale: number

	constructor(dimension: Dimension) {
		super(dimension)
		this.mainLayout = new FlowLayoutManager(undefined, {
			autoHeight: true,
			autoWidth: true,
			separator: dimension.height / 10,
		})
		this.infoLayout = new FlowLayoutManager(undefined, {
			direction: 'col',
			autoHeight: true,
			autoWidth: true,
			separator: dimension.height / 10,
		})
		this.playerInfo = {
			widget: new PlayerInfoWidget({width: dimension.width, height: dimension.height * 3 / 5}),
			visible: true,
		}
		this.playerAction = {
			widget: new PlayerActionWidget({width: dimension.width, height: dimension.height * 3 / 10}),
			visible: false
		}
		this.playerCards = {
			widget: new FlowLayoutManager(undefined, {
				autoHeight: true,
				autoWidth: true,
				separator: dimension.height / 16,
			}),
			visible: false
		}
		this.cardsScale = 1
		this.setVisibility()
	}

	set playerInfoVisibility(val: boolean) {
		this.playerInfo.visible = val
		this.setVisibility()
	}

	set playerActionVisibility(val: boolean) {
		this.playerAction.visible = val
		this.setVisibility()
	}

	set playerCardsVisibility(val: boolean) {
		this.playerCards.visible = val
		this.setVisibility()
	}

	set active(b: boolean) {
		if (b)
			this.playerInfo.widget.color = 'rgb(155,0,0)'
		else
			this.playerInfo.widget.color = 'rgb(0,0,0)'
	}

	private setVisibility() {
		this.infoLayout.removeAllWidget()
		if (this.playerInfo.visible)
			this.infoLayout.addWidget(this.playerInfo.widget)
		if (this.playerAction.visible)
			this.infoLayout.addWidget(this.playerAction.widget)

		this.mainLayout.removeAllWidget()
		if (this.playerCards.visible)
			this.mainLayout.addWidget(this.playerCards.widget)
		this.mainLayout.addWidget(this.infoLayout)
	}

	set username(newUsername: string) {
		this.playerInfo.widget.username = newUsername
	}

	set balance(value: number) {
		this.playerInfo.widget.balance = value
	}

	set actionName(action: Action) {
		this.playerAction.widget.action = action
	}

	set actionValue(value: number) {
		this.playerAction.widget.value = value
	}

	set cards(cards: Card[] | undefined) {
		this.playerCards.widget.removeAllWidget()
		if (cards === undefined) {
			return
		}
		for (const card of cards) this.playerCards.widget.addWidget(new CardWidget(this.cardsScale, card))
	}

	draw(ctx: CanvasRenderingContext2D) {
		this.mainLayout.draw(ctx)
	}
}

type Player = Position & { username?: string, id?: number, widget?: PlayerWidget }

export class PokerTable extends BaseWidget {
	private readonly root: BackgroundContainerWidget
	private readonly layout: FreeLayoutManager
	private readonly commonCardsLayout: FlowLayoutManager
	private readonly publicLayout: FlowLayoutManager
	private readonly potWidget: PotWidget
	private readonly players: Player[]
	private currentActivePlayerIndex?: number

	constructor() {
		super({width: 1864, height: 1080})
		this.layout = new FreeLayoutManager({width: 1864, height: 1080})
		this.root = new BackgroundContainerWidget(this.layout, background)

		this.publicLayout = new FlowLayoutManager(
			undefined, {autoHeight: true, autoWidth: true, direction: 'col', separator: 24})
		this.commonCardsLayout = new FlowLayoutManager(undefined, {
			separator: 24,
			autoWidth: true,
			autoHeight: true,
		})

		this.potWidget = new PotWidget({width: 250, height: 50}, 30)
		this.publicLayout.addWidget(this.potWidget)
		this.publicLayout.addWidget(this.commonCardsLayout)
		this.publicLayout.addWidget(new BaseWidget({width: 250, height: 50}))
		this.layout.addWidget(this.publicLayout, {x: this.dimension.width / 2, y: this.dimension.height / 2})

		this.players = new Array<Player>(5)
		this.players[0] = {x: 932 + Math.cos(90 * Math.PI / 180) * 540, y: 540 + Math.sin(90 * Math.PI / 180) * 540}
		this.players[1] = {x: 540 - Math.cos(45 * Math.PI / 180) * 540, y: 540 + Math.sin(45 * Math.PI / 180) * 540}
		this.players[2] = {x: 540 - Math.cos(45 * Math.PI / 180) * 540, y: 540 - Math.sin(45 * Math.PI / 180) * 540}
		this.players[3] = {x: 1324 + Math.cos(45 * Math.PI / 180) * 540, y: 540 - Math.sin(45 * Math.PI / 180) * 540}
		this.players[4] = {x: 1324 + Math.cos(45 * Math.PI / 180) * 540, y: 540 + Math.sin(45 * Math.PI / 180) * 540}
	}

	setPlayer(seat: number, username: string, balance: number) {
		if (this.players[seat].username !== undefined) {
			this.layout.removeWidget(this.players[seat].id!)
		}
		this.players[seat].widget = new PlayerWidget({width: 200, height: 200})
		this.players[seat].widget!.username = username
		this.players[seat].username = username
		this.players[seat].id = this.layout.addWidget(
			this.players[seat].widget!, {x: this.players[seat].x, y: this.players[seat].y})
	}

	private searchSeat(username: string): 0 | 1 | 2 | 3 | 4 | 5 {
		let i: number
		for (i = 0; i < this.players.length; i++) {
			const child = this.players[i]
			if (child.username === username)
				break
		}
		return i as 0 | 1 | 2 | 3 | 4 | 5
	}

	setPlayerBalance(username: string, balance: number) {
		const index = this.searchSeat(username)
		if (index === this.players.length)
			return
		this.players[index].widget!.balance = balance
	}

	setPlayerAction(username: string, action: Action, value: number) {
		const index = this.searchSeat(username)
		if (index === this.players.length)
			return
		this.players[index].widget!.actionName = action
		this.players[index].widget!.actionValue = value
		this.players[index].widget!.playerActionVisibility = true
	}

	setActivePlayer(username: string) {
		const index = this.searchSeat(username)
		if (index === this.players.length)
			return
		if (this.currentActivePlayerIndex !== undefined){
			const w = this.players[this.currentActivePlayerIndex].widget
			if (w !== undefined)
				w.active = false
		}
		this.players[index].widget!.active = true
	}

	setPlayerCards(username: string, cards: Card[], scale = 1) {
		const index = this.searchSeat(username)
		if (index === this.players.length)
			return
		this.players[index].widget!.cardsScale = scale
		this.players[index].widget!.cards = cards
		this.players[index].widget!.playerCardsVisibility = true
	}

	removePlayer(username: string) {
		const index = this.searchSeat(username)
		if (index === this.players.length)
			return
		this.layout.removeWidget(this.players[index].layoutID)
		this.players[index].widget = undefined
		this.players[index].username = undefined
		this.players[index].id = undefined
	}

	clearAllPlayersCards() {
		for (const player of this.players) {
			if (player.widget !== undefined) {
				player.widget.cards = undefined
				player.widget.playerCardsVisibility = false
			}
		}
	}

	clearAllPlayersActions() {
		for (const player of this.players)
			if (player.widget !== undefined)
				player.widget.playerActionVisibility = false
	}

	set commonCards(cards: Card[]) {
		this.commonCardsLayout.removeAllWidget()
		for (const card of cards) {
			this.commonCardsLayout.addWidget(new CardWidget(1, card))
		}
	}

	set pot(value: number) {
		this.potWidget.value = value
	}

	draw(ctx: CanvasRenderingContext2D) {
		super.draw(ctx)
		this.root.draw(ctx)
	}
}