import {
	Dimension,
	FreeLayoutManager,
	ImageWidget,
	MoneyWidget,
	RoundedRectangleWidget,
	TextWidget
} from './bison-canvas.js'

export class PlayerInfoWidget extends RoundedRectangleWidget {
	private child: FreeLayoutManager
	private readonly usernameWidget: TextWidget
	private readonly balanceWidget: MoneyWidget

	constructor(dimension: Dimension, username?: string, balance?: number) {
		super(dimension, 'black', dimension.height/3)
		this.child = new FreeLayoutManager(dimension, {XRelativeTo: 'left', YRelativeTo: 'top'})
		this.usernameWidget = new TextWidget({width: dimension.width, height: dimension.height/2}, dimension.height/4, 'white', username)
		this.balanceWidget = new MoneyWidget({width: dimension.width, height: dimension.height/2}, dimension.height/4, 'white', balance)
		this.child.addWidget(this.usernameWidget, {x: 0, y: 0})
		this.child.addWidget(this.balanceWidget, {x: 0, y: dimension.height/2})
	}

	set username(s: string){
		this.usernameWidget.text = s
	}

	set balance(v: number){
		this.balanceWidget.value = v
	}

	draw(ctx: CanvasRenderingContext2D) {
		super.draw(ctx)
		this.child.draw(ctx)
	}
}

export type CardNumber = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13
export type CardSuites = 'hearts' | 'spades' | 'diamonds' | 'clubs'
export type Card = {Number: CardNumber, Suit: CardSuites}
function cardToFileName(card: Card): string {
	return card.Number.toString().concat(card.Suit, '.png')
}
const cards: Map<string, HTMLImageElement> = new Map<string, HTMLImageElement>()
const loadCards = () => {
	const root = document.body.getAttribute('data-bg-root')
	const suites = ['spades', 'hearts', 'diamonds', 'clubs']
	for (let i = 0; i < 4; i++) {
		for (let j = 0; j < 13; j++) {
			let img = new Image()
			img.src = root + 'bg-res/standard-cards/' + cardToFileName({Number: (j+1) as CardNumber, Suit: suites[i] as CardSuites})
			cards.set((j+1) + suites[i], img)
		}
	}
}
loadCards()


export class CardWidget extends ImageWidget {
	constructor(scale: number, card: Card) {
		super({width: 128 * scale, height: 208 * scale}, cards.get(card.Number + card.Suit)!)
	}

	draw(ctx: CanvasRenderingContext2D) {
		super.draw(ctx)
	}
}