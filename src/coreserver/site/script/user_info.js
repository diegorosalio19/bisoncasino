const getInfo = () => {
  let token = localStorage.getItem("token")
  let username = localStorage.getItem("username")
  if (token == null) return null
  let request = new XMLHttpRequest()
  request.open("GET", "http://" + SERVER_ADDRESS + "/api/v1/user/" + username + "/", false)
  request.setRequestHeader("X-Token", token)
  request.send()
  if (request.status === 200) {
    let obj = JSON.parse(request.responseText)
    return {
      Username: obj.Username,
      Balance: obj.Balance
    }
  } else {
    localStorage.removeItem("token")
    localStorage.removeItem("username")
    return null
  }
  s
}