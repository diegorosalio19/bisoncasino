const LoginRequest = (form) => {
  let req = new XMLHttpRequest()
  req.open("POST", "http://" + SERVER_ADDRESS + "/api/v1/user/login/", true)
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
  req.onreadystatechange = () => {
    let obj = JSON.parse(req.responseText)
    if (req.readyState === 4 && req.status === 202) {
      form.classList.remove('was-validated')
      localStorage.setItem("token", obj.Token.toString())
      localStorage.setItem("username", form.Username.value)
      window.location.replace("./index.html");
    } else if (req.readyState === 4 && req.status === 406) {
      if (obj.UsernameInvalid === true) {
        document.getElementById("username").classList.add("is-invalid")
      }
      if (obj.PasswordInvalid === true) {
        document.getElementById("password").classList.add("is-invalid")
      }
    }
  }
  req.send("Username=" + form.Username.value + "&Password=" + form.Password.value)
}

const SignupRequest = (form) => {
  let req = new XMLHttpRequest()
  req.open("POST", "http://" + SERVER_ADDRESS + "/api/v1/user/", true)
  req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded")
  req.onreadystatechange = () => {
    if (req.readyState === 4 && req.status === 201) {
      window.location.replace("./login.html");
    } else if (req.readyState === 4 && req.status === 406) {
      document.getElementById("username").classList.add("is-invalid")
      document.getElementById("password").classList.add("is-invalid")
    }
  }
  req.send("Username=" + form.Username.value + "&Password=" + form.Password.value)
}

const LogoutRequest = () => {
  let token = localStorage.getItem("token")
  let username = localStorage.getItem("username")
  if (token == null) return
  let request = new XMLHttpRequest()
  request.open("POST", "http://" + SERVER_ADDRESS + "/api/v1/user/" + username + "/logout", false)
  request.setRequestHeader("X-Token", token)
  request.send()
  localStorage.removeItem("token")
  localStorage.removeItem("username")
}

const initForm = (func) => {
  const forms = document.querySelectorAll('.needs-validation')
  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
        form.classList.add('was-validated')
      } else {
        event.preventDefault()
        event.stopPropagation()
        func(form)
      }
    }, false)
  })
}