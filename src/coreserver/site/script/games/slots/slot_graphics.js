const SPIN_SPEED = 0.4

// const example = {
//     canvasID: "example",
//     maxDimension: {
//         width: 0,
//         height: 0,
//     },
//     columnNumber: 0,
//     rowNumber: 0,
//     blockingLine: 0,
//     elementDimension: 0,
//     resources: {
//         columnElements: [],
//         linesBackgroundImage: null,
//     }
// }

class SlotGraphicsEngine {
    constructor(graphicOptions) {
        this.graphicOptions = {...graphicOptions}

        if (this.graphicOptions.maxDimension !== undefined) {
            const md = this.graphicOptions.maxDimension
            if (md.height / this.graphicOptions.rowNumber > md.width / this.graphicOptions.columnNumber){
                this.graphicOptions.elementDimension = md.width / this.graphicOptions.columnNumber
            } else {
                this.graphicOptions.elementDimension = md.height / this.graphicOptions.rowNumber
            }
        }

        let canvas = document.getElementById(this.graphicOptions.canvasID);
        canvas.width = this.graphicOptions.elementDimension * this.graphicOptions.columnNumber
        canvas.height = this.graphicOptions.elementDimension * this.graphicOptions.rowNumber
        this.ctx = canvas.getContext("2d")

        this.lines = Array(this.graphicOptions.columnNumber)
        for (let i = 0; i < this.lines.length; i++) {
            this.lines[i] = new Line({
                visibleElementsNumber: this.graphicOptions.rowNumber,
                blockingLine: this.graphicOptions.blockingLine,
                elements: this.graphicOptions.resources.columnElements.slice(0),
                drawSettings: {
                    elementsDimension: this.graphicOptions.elementDimension,
                    spinningWheelRoot: {
                        x: 0, y: 0
                    },
                    ctx: this.ctx,
                    GetElementPos: (rowNumber) => {
                        return {
                            x: i * this.graphicOptions.elementDimension,
                            y: rowNumber * this.graphicOptions.elementDimension
                        }
                    }
                }
            })
        }
        this.LineCatched = 0
    }
    Reset(comb){
        this.lines.forEach((el, ix)=>{
            el.Reset(comb[ix])
        })
        this.LineCatched = 0
    }
    SetOnCompareID(fn){
        this.lines.forEach((el)=>{
            el.OnCompareID = fn
        })
    }
    SetOnCatchedElement(fn){
        this.lines.forEach((el)=>{
            el.OnCatchedElement = fn
        })
    }
    Spin(){
        this.lines.forEach((el)=>{
            let catched = el.SpinOneTick()
            if (catched) this.LineCatched++
        })
    }
    Draw(){
        const ctx = this.ctx
        ctx.clearRect(0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight)
        ctx.drawImage(this.graphicOptions.resources.linesBackgroundImage, 0, 0, ctx.canvas.clientWidth, ctx.canvas.clientHeight)
        this.lines.forEach((el)=>{
            el.Draw()
        })
    }
}

class Line {
    constructor(settings) {
        this.settings = {...settings}
        this.spin = {offset: 0, speed: SPIN_SPEED};
        this.elements = shuffle(this.settings.elements)
        this.visibleElements = this.elements.slice(0, this.settings.visibleElementsNumber+2)
        this.blockID = {};

        // Callbacks
        this.OnCompareID = undefined
        this.OnCatchedElement = undefined
    }
    Reset(blockID){
        this.shuffleNotVisible();
        this.blockID = {...blockID};
        this.spin = {offset: 0, speed: SPIN_SPEED};
    }
    SpinOneTick(){
        let catched = false
        this.spin.offset -= this.spin.speed
        if (this.spin.offset <= -1.0){
            catched = this.generate();
            this.spin.offset = 0;
        }
        return catched
    }
    Draw(){
        this.visibleElements.forEach((el, ix)=>{
            let elPos = this.settings.drawSettings.GetElementPos(ix-1);
            elPos.x += this.settings.drawSettings.spinningWheelRoot.x
            elPos.y += this.settings.drawSettings.spinningWheelRoot.y + this.spin.offset * this.settings.drawSettings.elementsDimension
            el.Draw(elPos.x, elPos.y, this.settings.drawSettings.elementsDimension, this.settings.drawSettings.ctx);
        })
    }
    shuffleNotVisible(){
        this.elements = this.visibleElements.concat(
            shuffle(this.elements.slice(this.settings.visibleElementsNumber+2, this.elements.length))
        );
    }
    generate(){
        this.elements.push(
            this.elements.shift()
        )
        this.visibleElements = this.elements.slice(0, this.settings.visibleElementsNumber+2);
        checkObjectUndefinedNull(this.OnCompareID, "OnCompareID")
        if (this.OnCompareID(this.visibleElements[this.settings.blockingLine + 1].GetID(), this.blockID)) {
            checkObjectUndefinedNull(this.OnCatchedElement, "OnCatchedElement")
            this.OnCatchedElement(this)
            return true
        }
    }
}

class SlotCard {
    constructor(id, image) {
        this.id = id
        this.image = image
    }
    GetID(){
        return this.id
    }
    Draw(hOff, vOff, dimension, ctx){
        if (this.image.complete){
            ctx.drawImage(this.image, hOff, vOff, dimension, dimension)
        }
    }
}

function shuffle(array) {
    let currentIndex = array.length,  randomIndex;
    while (currentIndex !== 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex], array[currentIndex]];
    }
    return array;
}

function checkObjectUndefinedNull(obj, objectName){
    if (obj === null){
        console.error(objectName + " is null")
    } else if (obj === undefined){
        console.error(objectName + " is undefined")
    }
}