const SUIT_NAMES = ["spades", "hearts", "diamonds", "clubs"]
const PATH_TO_RESOURCES = "./resources/slot/PokerSlot/"

class PokerSlot{
    constructor(dimension, canvasID, userToken) {
        const gr_opt = {
            canvasID: canvasID,
            columnNumber: 5,
            rowNumber: 3,
            blockingLine: 1,
            maxDimension: dimension,
            resources: {
                columnElements: fetchPlayingCards(PATH_TO_RESOURCES + "tiles/"),
                linesBackgroundImage: fetchPokerLinesBackgroundImage(PATH_TO_RESOURCES)
            }
        }
        this.graphicsEngine = new SlotGraphicsEngine(gr_opt)
        this.graphicsEngine.SetOnCompareID((a, b) => {
            return a.Number === b.Number && a.Suit === b.Suit
        })
        this.graphicsEngine.SetOnCatchedElement((line) => {
            line.spin.speed = 0
        })

        this.socket = new WebSocket('ws://'+SERVER_ADDRESS+'/api/v1/game/slot_machine/');
        this.socket.addEventListener('open', () => {
            this.socket.send(userToken);
        });
        this.socket.addEventListener('message', (event) => {
            let obj = JSON.parse(event.data)
            this.onGameResultObtained(obj)
        })
    }
    Play(onPlayExit = ()=>{}){
        this.onPlayExit = onPlayExit
        this.socket.send(JSON.stringify({Bet: 1}))
    }
    onGameResultObtained(data){
        const comb = data.Cards
        this.graphicsEngine.Reset(comb)
        const pLoop = ()=>{
            this.graphicsEngine.Spin()
            this.graphicsEngine.Draw()
            if (this.graphicsEngine.LineCatched !== 5)
                window.requestAnimationFrame(pLoop)
            else {
                // if (data.Combination !== "HighCard") {
                //     this.ctx.font = '100px sans-serif';
                //     const pos = {
                //         x: 0,
                //         y: this.ctx.canvas.clientHeight / 2+50,
                //     }
                //     this.ctx.fillText(transformWinText(data.Combination), pos.x, pos.y);
                // }
                this.onPlayExit(data)
            }
        }
        window.requestAnimationFrame(pLoop)
    }
    Exit(){
        this.socket.close()
    }
}

function transformWinText(str){
    let result = str.split(/(?=[A-Z])/);
    let r = "";
    result.forEach((el)=>{
        r+= el.toUpperCase() + " "
    });
    return r.slice(0, r.length-1);
}

function fetchPokerLinesBackgroundImage(pathResourceDirectory) {
    let img = new Image();
    img.src = pathResourceDirectory + "tiles-background.png";
    return img;
}

function fetchPlayingCards(pathResourceDirectory){
    let elements = Array(4*13)
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 13; j++) {
            let img = new Image()
            let fileName = (j+1)+SUIT_NAMES[i]
            img.src = pathResourceDirectory + fileName + ".png"
            elements[i*13+j] = new SlotCard(
                {
                        Number: j+1,
                        Suit: SUIT_NAMES[i].charAt(0).toUpperCase() + SUIT_NAMES[i].slice(1)
                },
                img
            )
        }
    }
    return elements
}
