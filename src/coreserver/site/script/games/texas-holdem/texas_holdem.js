class PokerTable {
	constructor(parentID, userToken, sessBal, tableID) {
		this._canvas = new BisonCanvas.Canvas(parentID, 'rgba(0,0,0,0)', {width: 1864, height: 1080})
		this._engine = new BisonPoker.TableLayout({bg: ResourceLoader.PokerTable, actionLib: ResourceLoader.Actions, cardsLib: ResourceLoader.Cards})
		this._canvas.root = this._engine
		window.onresize = () => {
			this._canvas.resize()
		}
		this.onTurnStart = null
		this.onTurnEnd = null
		this.socket = new WebSocket('ws://' + SERVER_ADDRESS + '/api/v1/game/poker_texas_holdem/?session_balance=' + sessBal + '&table_id=' + tableID);
		this.socket.addEventListener('open', () => {
			this.socket.send(userToken);
			this._engine.setPlayer(0, localStorage.getItem('username'), parseFloat(sessBal))
		});
		this.socket.addEventListener('message', (event) => {
			let e = JSON.parse(event.data)
			this.onGameEvent(e)
		})
	}

	Start() {
		this._canvas.animate()
	}

	Exit() {
		this.socket.close()
		// delete canvas
	}

	Play(obj) {
		this.socket.send(JSON.stringify(obj))
		this.onTurnEnd()
	}

	_cardsToStringArr(cards){
		const car = []
		for (const card of cards) {
			car.push(card.Number + card.Suit.toLowerCase())
		}
		return car
	}

	onGameEvent(e) {
		switch (e.NewsType) {
			case "PlayerPlayed":
				this._engine.setPlayerAction(e.Username, e.Action, e.TotalBet)
				break
			case "PotChanged":
				this._engine.pot = e.Value;
				this._engine.clearAllPlayersActions()
				break
			case "PrivateCardsObtained":
				this._engine.setPlayerCards(localStorage.getItem('username'), this._cardsToStringArr(e.Cards))
				break
			case "PublicCardsObtained":
				this._engine.commonCards = this._cardsToStringArr(e.Cards)
				this._engine.clearAllPlayersActions()
				break
			case "Outcome":
				let str = ''
				let winnersNumber = 0
				for (const name in e.Winners) {
					str += name + ", "
					winnersNumber++
				}
				str = str.substring(0, str.length - 2)
				str += ' '
				if (winnersNumber > 1){
					str += 'have '
				}
				str += 'made ' + e.Combination.CombinationMajor + '!'
				//this.engine.SetOutcomeMessage(str)
				break
			case "PlayersList":
				this._engine.clearAllPlayers()
				let playerIdx, j = 0
				for (playerIdx = 0; playerIdx < e.Players.length; playerIdx++) {
					if (window.localStorage.getItem("username") === e.Players[playerIdx].Username)
						break
				}
				this._engine.setPlayer(0, e.Players[playerIdx].Username, e.Players[playerIdx].Balance)
				for (let i = playerIdx+1; i < e.Players.length + playerIdx; i++) {
					this._engine.setPlayer(j++, e.Players[i % e.Players.length].Username, e.Players[i % e.Players.length].Balance)
				}
				break
			case "CardsShowed":
				for (const key in e.Map) {
					this._engine.setPlayerCards(key, this._cardsToStringArr(e.Map[key]), 0.5)
				}
				break
			case "MemberJoin":
				break
			case "MemberQuit":
				this._engine.removePlayer(e.Username)
				break
			case "PlayerPlaying":
				this._engine.setActivePlayer(e.Username)
				break
			case "PlayerBalanceChanged":
				this._engine.setPlayerBalance(e.Username, e.Value)
				break
			case "Play":
				this.onTurnStart(e.MinBet, e.LastBet)
				break
			case "GameStarted":
				this._engine.clearAllPlayersCards()
				this._engine.commonCards = []
				break
			case "GameEnded":
				this._engine.clearAllPlayersActions()
				// this._engine.ClearOutcomeMessage()
				this._engine.clearAllPlayersCards()
				break
		}
		console.log(e)
	}
}