import {PokerTable} from '../../bisoncanvas/bison-poker.js'
import {Canvas} from '../../bisoncanvas/bison-canvas.js'
import {Card} from '../../bisoncanvas/bison-games'

const SERVER_ADDRESS = 'localhost'

interface News {
	NewsType: string
	Username?: string
	Action?: 'fold' | 'call' | 'recall' | 'check'
	TotalBet?: number
	Value?: number
	Cards?: Card[]
	Map?: { [s: string]: Card[] }
	Combination?: { CombinationMajor: 'HighCard' | 'Pair' | 'TwoPair' | 'ThreeOfAKind' | 'Straight' | 'Flush' | 'FullHouse' | 'FourOfAKind' | 'StraightFlush', Score: number }
	Winners?: { [s: string]: number }
	Players?: { Username: string, Balance: number }[]
	MinBet?: number
	LastBet?: number
	PlayerAction?: 'fold' | 'call' | 'recall' | 'check'
	RecallValue?: number
}

export function createTable() {
	const inp = document.getElementById('big-blind-input')
	if (inp === null) return
	const bb = parseInt((inp as HTMLInputElement).value)
	let req = new XMLHttpRequest()
	req.open('POST', 'http://' + SERVER_ADDRESS + '/api/v1/game/poker_texas_holdem/?big_blind=' + bb, true)
	req.onreadystatechange = () => {
		if (req.readyState === 4 && req.status === 201) {
			const ti = document.getElementById('table-id-input')
			if (ti === null) return
			(ti as HTMLInputElement).value = JSON.parse(req.responseText).ID
		}
	}
	req.send()
}

export class PokerTexasHoldemController {
	private readonly view: PokerTable
	private readonly parent: HTMLElement
	private readonly canvas: Canvas
	private readonly socket: WebSocket

	private foldButton: HTMLButtonElement
	private recallButton: HTMLButtonElement
	private recallValue: HTMLInputElement
	private callButton: HTMLButtonElement
	private checkButton: HTMLButtonElement
	private minBet: number

	constructor(parentID: string, userToken: string, sessBal: number, tableID: string) {
		this.parent = document.getElementById(parentID)!
		this.canvas = new Canvas(parentID, 'white', {width: 1864, height: 1080})
		this.parent.insertAdjacentElement('beforeend', this.createButtonController())
		this.view = new PokerTable()
		this.canvas.root = this.view
		this.canvas.start()
		window.onresize = () => this.canvas.resize()
		window.onbeforeunload = () => this.exit()
		this.socket = new WebSocket('ws://' + SERVER_ADDRESS + '/api/v1/game/poker_texas_holdem/?session_balance=' + sessBal + '&table_id=' + tableID)
		this.socket.addEventListener('open', () => {
			this.socket.send(userToken)
			this.view.setPlayer(0, localStorage.getItem('username')!, sessBal)
		})
		this.socket.addEventListener('message', (event) => {
			let e = JSON.parse(event.data)
			this.onGameEvent(e)
		})
	}

	private createButtonController(): HTMLElement {
		const main = document.createElement('div')
		main.classList.add('container', 'py-3')

		const inpGroup = document.createElement('div')
		inpGroup.classList.add('input-group', 'text-center', 'row')
		main.appendChild(inpGroup)

		this.foldButton = document.createElement('button')
		this.foldButton.classList.add('col', 'fw-semibold', 'btn', 'btn-sm', 'btn-danger')
		this.foldButton.type = 'button'
		this.foldButton.innerText = 'FOLD'
		this.foldButton.disabled = true
		this.foldButton.addEventListener('click', this.fold.bind(this))
		inpGroup.appendChild(this.foldButton)

		this.recallButton = document.createElement('button')
		this.recallButton.classList.add('col', 'fw-semibold', 'btn', 'btn-sm', 'btn-warning')
		this.recallButton.type = 'button'
		this.recallButton.innerText = 'RECALL'
		this.recallButton.disabled = true
		this.recallButton.addEventListener('click', this.recall.bind(this))
		inpGroup.appendChild(this.recallButton)

		this.recallValue = document.createElement('input')
		this.recallValue.classList.add('form-control', 'col')
		this.recallValue.type = 'number'
		this.recallValue.disabled = true
		this.recallValue.placeholder = 'Amount'
		inpGroup.appendChild(this.recallValue)

		this.callButton = document.createElement('button')
		this.callButton.classList.add('col', 'fw-semibold', 'btn', 'btn-sm', 'btn-info')
		this.callButton.type = 'button'
		this.callButton.innerText = 'CALL'
		this.callButton.disabled = true
		this.callButton.addEventListener('click', this.call.bind(this))
		inpGroup.appendChild(this.callButton)

		this.checkButton = document.createElement('button')
		this.checkButton.classList.add('col', 'fw-semibold', 'btn', 'btn-sm', 'btn-success')
		this.checkButton.type = 'button'
		this.checkButton.innerText = 'CHECK'
		this.checkButton.disabled = true
		this.checkButton.addEventListener('click', this.check.bind(this))
		inpGroup.appendChild(this.checkButton)

		return main
	}

	private onTurnStart(MinBet: number, LastBet: number) {
		this.foldButton.disabled = false
		this.recallButton.disabled = false
		this.recallValue.disabled = true
		this.callButton.disabled = false
		this.checkButton.disabled = false

		this.minBet = MinBet
		if (MinBet - LastBet === 0) {
			this.callButton.setAttribute('disabled', 'true')
			this.callButton.innerText = 'CALL'
		} else {
			this.checkButton.setAttribute('disabled', 'true')
			this.callButton.innerText = 'CALL ' + (MinBet - LastBet) + '€'
		}
	}

	private onTurnEnd() {
		this.foldButton.disabled = true
		this.recallButton.disabled = true
		this.recallValue.disabled = true
		this.callButton.disabled = true
		this.checkButton.disabled = true
		this.callButton.innerText = 'CALL'
	}

	fold() {
		this.socket.send(JSON.stringify({PlayerAction: 'fold'}))
		this.onTurnEnd()
	}

	recall() {
		if (this.recallValue.disabled) {
			this.recallValue.min = '0'
			this.recallValue.disabled = false
		} else {
			this.socket.send(JSON.stringify({
				PlayerAction: 'recall',
				RecallValue: this.minBet + parseInt(this.recallValue.value)
			}))
			this.recallValue.disabled = true
		}
		this.socket.send(JSON.stringify({PlayerAction: 'fold'}))
		this.onTurnEnd()
	}

	call() {
		this.socket.send(JSON.stringify({PlayerAction: 'call'}))
		this.onTurnEnd()
	}

	check() {
		this.socket.send(JSON.stringify({PlayerAction: 'check'}))
		this.onTurnEnd()
	}

	exit() {
		this.socket.close()
		this.canvas.destroy()
		this.parent.remove()
		window.onresize = () => {
		}
	}

	onGameEvent(e: News) {
		switch (e.NewsType) {
			case 'PlayerPlayed':
				this.view.setPlayerAction(e.Username!, e.Action!, e.TotalBet!)
				break
			case 'PotChanged':
				this.view.pot = e.Value!
				this.view.clearAllPlayersActions()
				break
			case 'PrivateCardsObtained':
				this.view.setPlayerCards(localStorage.getItem('username')!, e.Cards!)
				break
			case 'PublicCardsObtained':
				this.view.commonCards = e.Cards!
				this.view.clearAllPlayersActions()
				break
			case 'Outcome':
				let str = ''
				let winnersNumber = 0
				for (const name in e.Winners) {
					str += name + ', '
					winnersNumber++
				}
				str = str.substring(0, str.length - 2)
				str += ' '
				if (winnersNumber > 1) {
					str += 'have '
				}
				str += 'made ' + e.Combination!.CombinationMajor + '!'
				console.log(str)
				break
			case 'PlayersList':
				let playerIdx, j = 0
				for (playerIdx = 0; playerIdx < e.Players!.length; playerIdx++) {
					if (window.localStorage.getItem('username') === e.Players![playerIdx].Username)
						break
				}
				for (let i = 0; i < e.Players!.length; i++) {
					const player = e.Players![(playerIdx + i) % e.Players!.length]
					this.view.setPlayer(i % 5, player.Username, player.Balance)
				}
				break
			case 'CardsShowed':
				for (const key in e.Map) {
					this.view.setPlayerCards(key, e.Map[key], 0.5)
				}
				break
			case 'MemberJoin':
				break
			case 'MemberQuit':
				this.view.removePlayer(e.Username!)
				break
			case 'PlayerPlaying':
				this.view.setActivePlayer(e.Username!)
				break
			case 'PlayerBalanceChanged':
				this.view.setPlayerBalance(e.Username!, e.Value!)
				break
			case 'Play':
				this.onTurnStart(e.MinBet!, e.LastBet!)
				break
			case 'GameStarted':
				this.view.clearAllPlayersCards()
				this.view.commonCards = []
				break
			case 'GameEnded':
				this.view.clearAllPlayersActions()
				// this._engine.ClearOutcomeMessage()
				this.view.clearAllPlayersCards()
				break
		}
		console.log(e)
	}
}