package users

import apiusers "bisoncasino.net/api/user"

type User struct {
	Username       string
	HashedPassword string
	Balance        float32
}

func (u *User) GetInfo() apiusers.UserInfo {
	return apiusers.UserInfo{
		Username: u.Username,
		Balance:  u.Balance,
	}
}

func (u *User) GetPublicInfo() apiusers.UserPublicInfo {
	return apiusers.UserPublicInfo{
		Username: u.Username,
		Balance:  u.Balance,
	}
}
