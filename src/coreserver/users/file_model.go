package users

import (
	apiusers "bisoncasino.net/api/user"
	"encoding/json"
	"github.com/google/uuid"
	"log"
	"os"
	"sync"
)

type fileModel struct {
	mx         sync.Mutex
	filePath   string
	userMap    map[string]*User
	activeUser map[string]*User
}

func NewFileUserManager(path string) UserManager {
	f := &fileModel{
		mx:         sync.Mutex{},
		filePath:   path,
		userMap:    nil,
		activeUser: make(map[string]*User),
	}
	var err error
	f.userMap, err = loadUsers(f.filePath)
	if err != nil {
		f.userMap = make(map[string]*User)
		log.Printf("error reading %s file: %s\n", f.filePath, err)
	}
	return f
}

func (f *fileModel) Login(username, hashedPassword string) (token string, err error) {
	f.mx.Lock()
	if user, found := f.userMap[username]; found {
		if user.HashedPassword == hashedPassword {
			token = uuid.New().String()
			f.activeUser[token] = user
		} else {
			err = WrongPasswordError{}
		}
	} else {
		err = UsernameNotFoundError{}
	}
	f.mx.Unlock()
	return
}

func (f *fileModel) Logout(token string) (err error) {
	f.mx.Lock()
	if _, found := f.activeUser[token]; found {
		_ = saveUsers(f.filePath, f.userMap)
		delete(f.activeUser, token)
	} else {
		err = TokenNotFoundError{}
	}
	f.mx.Unlock()
	return
}

func (f *fileModel) Signup(username, hashedPassword string) (err error) {
	f.mx.Lock()
	if _, found := f.userMap[username]; found {
		err = UsernameAlreadyExistError{}
	} else {
		f.userMap[username] = &User{
			Username:       username,
			HashedPassword: hashedPassword,
			Balance:        20000,
		}
	}
	f.mx.Unlock()
	_ = saveUsers(f.filePath, f.userMap)
	return
}

func (f *fileModel) Delete(token string) (err error) {
	f.mx.Lock()
	if user, found := f.activeUser[token]; found {
		delete(f.userMap, user.Username)
		_ = saveUsers(f.filePath, f.userMap)
		delete(f.activeUser, token)
	} else {
		err = TokenNotFoundError{}
	}
	f.mx.Unlock()
	return
}

func (f *fileModel) GetUser(token string) (user *User, err error) {
	if u, found := f.activeUser[token]; found {
		user = u
	} else {
		err = TokenNotFoundError{}
	}
	return
}

func (f *fileModel) GetUserPublicInfo(username string) (userInfo apiusers.UserPublicInfo, err error) {
	user, found := f.userMap[username]
	if !found {
		err = UsernameNotFoundError{}
		return
	}
	userInfo = user.GetPublicInfo()
	return
}

func (f *fileModel) GetUserPublicInfoList() (users []apiusers.UserPublicInfo) {
	users = make([]apiusers.UserPublicInfo, 0, len(f.userMap))
	for _, user := range f.userMap {
		users = append(users, user.GetPublicInfo())
	}
	return
}

func saveUsers(path string, um map[string]*User) error {
	file, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)
	enc := json.NewEncoder(file)
	values := make([]User, 0, len(um))
	for _, v := range um {
		values = append(values, *v)
	}
	err = enc.Encode(values)
	if err != nil {
		return err
	}
	return nil
}

func loadUsers(path string) (map[string]*User, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)
	dec := json.NewDecoder(file)
	var decodedValue []User
	err = dec.Decode(&decodedValue)
	if err != nil {
		return nil, err
	}
	return sliceToUserMap(decodedValue), nil
}

func sliceToUserMap(values []User) map[string]*User {
	um := make(map[string]*User)
	for _, value := range values {
		valueCopy := value
		um[value.Username] = &valueCopy
	}
	return um
}
