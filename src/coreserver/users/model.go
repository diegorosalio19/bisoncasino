package users

import (
	apiusers "bisoncasino.net/api/user"
)

type WrongPasswordError struct{}

func (receiver WrongPasswordError) Error() string {
	return "wrong password"
}

type UsernameNotFoundError struct{}

func (receiver UsernameNotFoundError) Error() string {
	return "username not found"
}

type TokenNotFoundError struct{}

func (receiver TokenNotFoundError) Error() string {
	return "token not found"
}

type UsernameAlreadyExistError struct{}

func (receiver UsernameAlreadyExistError) Error() string {
	return "username already exist"
}

type BadPageNumber struct{}

func (BadPageNumber) Error() string {
	return "page number not found"
}

type UserManager interface {
	Login(username, hashedPassword string) (token string, err error)
	Logout(token string) (err error)
	Signup(username, hashedPassword string) (err error)
	Delete(token string) (err error)
	GetUser(token string) (user *User, err error)
	GetUserPublicInfo(username string) (users apiusers.UserPublicInfo, err error)
	GetUserPublicInfoList() (users []apiusers.UserPublicInfo)
}
