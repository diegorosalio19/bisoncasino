package users

import (
	"bisoncasino.net/api"
	apiusers "bisoncasino.net/api/user"
	"encoding/json"
	"errors"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func GetAPIHandler(u UserManager) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {
		//if request.Method == http.MethodOptions {
		//	sendOptions(writer, request)
		//}
		api.InitJSONPublicResponse(writer)
		var fields = api.ParseField(request.URL.Path, "/user/")
		switch len(fields) {
		case 0:
			switch request.Method {
			case http.MethodGet:
				serviceUsersList(u, writer, request)
			case http.MethodPost:
				serviceUserSignup(u, writer, request)
			}
		case 1:
			if fields[0] == "login" {
				switch request.Method {
				case http.MethodPost:
					serviceUserLogin(u, writer, request)
				}
			} else {
				switch request.Method {
				case http.MethodGet:
					serviceUserInfo(u, fields[0], writer, request)
				case http.MethodDelete:
					serviceUserDelete(u, writer, request)
				}
			}
		case 2:
			if fields[1] == "logout" {
				switch request.Method {
				case http.MethodPost:
					serviceUserLogout(u, writer, request)
				}
			}
		}
	}
}

func serviceUsersList(u UserManager, writer http.ResponseWriter, request *http.Request) {
	pag := NewPaginatorFromQuery(request.URL.Query())
	list, totalPages, err := pag.Paginate(u.GetUserPublicInfoList())
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	response := apiusers.UserListResponse{
		PerPage:    pag.perPage,
		Page:       pag.page,
		TotalPages: totalPages,
		Users:      list,
	}
	_ = json.NewEncoder(writer).Encode(response)
}

func serviceUserSignup(u UserManager, writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	username, password, authError := processAuthForm(request.Form)
	if authError != nil {
		writer.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(writer).Encode(*authError)
		return
	}
	if u.Signup(username, password) != nil {
		authError = &apiusers.AuthError{
			UsernameInvalid: true,
		}
		writer.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(writer).Encode(*authError)
		return
	}
	writer.WriteHeader(http.StatusCreated)
}
func serviceUserDelete(u UserManager, writer http.ResponseWriter, request *http.Request) {
	token := api.GetToken(request)
	err := u.Delete(token)
	if err != nil {
		writer.WriteHeader(http.StatusUnauthorized)
		return
	}
	writer.WriteHeader(http.StatusOK)
}
func serviceUserLogin(u UserManager, writer http.ResponseWriter, request *http.Request) {
	err := request.ParseForm()
	if err != nil {
		http.Error(writer, err.Error(), http.StatusBadRequest)
		return
	}
	username, password, authError := processAuthForm(request.Form)
	if authError != nil {
		writer.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(writer).Encode(*authError)
		return
	}
	if token, err := u.Login(username, password); err != nil {
		authError = new(apiusers.AuthError)
		switch err.(type) {
		case UsernameNotFoundError:
			authError.UsernameInvalid = true
		case WrongPasswordError:
			authError.PasswordInvalid = true
		}
		writer.WriteHeader(http.StatusNotAcceptable)
		_ = json.NewEncoder(writer).Encode(*authError)
	} else {
		writer.WriteHeader(http.StatusAccepted)
		_ = json.NewEncoder(writer).Encode(apiusers.LoginResponse{Token: token}) // TODO Set-Cookie
	}
}
func serviceUserLogout(u UserManager, writer http.ResponseWriter, request *http.Request) {
	token := api.GetToken(request)
	err := u.Logout(token)
	if err != nil {
		writer.WriteHeader(http.StatusUnauthorized)
		return
	}
	writer.WriteHeader(http.StatusOK)
}
func serviceUserInfo(u UserManager, username string, writer http.ResponseWriter, request *http.Request) {
	token := api.GetToken(request)
	if token == "" {
		info, err := u.GetUserPublicInfo(username)
		if err != nil {
			writer.WriteHeader(http.StatusBadRequest)
			return
		}
		writer.WriteHeader(http.StatusPartialContent)
		_ = json.NewEncoder(writer).Encode(info)
		return
	}
	user, err := u.GetUser(token)
	if err != nil {
		writer.WriteHeader(http.StatusUnauthorized)
		return
	}
	writer.WriteHeader(http.StatusOK)
	_ = json.NewEncoder(writer).Encode(user.GetInfo())
}

type Paginator struct {
	page     int
	perPage  int
	username string
}

func NewPaginatorFromQuery(values url.Values) Paginator {
	var str string
	var pag = Paginator{0, 3, ""}
	if values.Has(apiusers.URLParametersPage) {
		str = values.Get(apiusers.URLParametersPage)
		r, err := strconv.ParseInt(str, 10, 32)
		if err == nil {
			pag.page = int(r)
		}
	}
	if values.Has(apiusers.URLParametersPerPage) {
		str = values.Get(apiusers.URLParametersPerPage)
		r, err := strconv.ParseInt(str, 10, 32)
		if err == nil {
			pag.perPage = int(r)
		}
	}
	if values.Has(apiusers.URLParametersUsername) {
		str = values.Get(apiusers.URLParametersUsername)
		pag.username = str
	}
	return pag
}
func (p *Paginator) Paginate(info []apiusers.UserPublicInfo) (page []apiusers.UserPublicInfo, totalPages int, err error) {
	if p.perPage == 0 {
		err = errors.New("invalid perPage")
		return
	}
	totalPages = len(info) / p.perPage
	if len(info)%p.perPage != 0 {
		totalPages++
	}
	if p.page < 0 || p.page >= totalPages {
		err = errors.New("page out of bound")
		return
	}
	page = info[p.page*p.perPage:]
	if len(page) > p.perPage {
		page = page[:p.perPage]
	}
	return
}

func processAuthForm(values url.Values) (username, password string, authError *apiusers.AuthError) {
	authError = &apiusers.AuthError{}
	var errorCache bool
	username = strings.TrimSpace(values.Get(apiusers.FormParametersUsername))
	if username == "" || username == "login" {
		errorCache = true
		authError.UsernameInvalid = true
	}
	password = strings.TrimSpace(values.Get(apiusers.FormParametersPassword))
	if password == "" {
		errorCache = true
		authError.PasswordInvalid = true
	}
	if !errorCache {
		authError = nil
	}
	return
}
