package user

const (
	URLParametersPage      = "page"
	URLParametersPerPage   = "per_page"
	URLParametersUsername  = "username"
	FormParametersUsername = "Username"
	FormParametersPassword = "Password"
	//CookieParametersToken  = "Token"
)

type UserInfo struct {
	Username string
	Balance  float32
}

type UserPublicInfo struct {
	Username string
	Balance  float32
}

type UserListResponse struct {
	PerPage, Page, TotalPages int
	Users                     []UserPublicInfo
}

type UserCreatedResponse struct {
	Message string
}

type AuthError struct {
	UsernameInvalid, PasswordInvalid bool
}

type LoginResponse struct {
	Token string
}
