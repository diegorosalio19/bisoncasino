package news

import "encoding/json"

type News []byte

const (
	memberJoinNews = "MemberJoin"
	memberQuitNews = "MemberQuit"
)

func MemberJoin(username string) News {
	r, _ := json.Marshal(struct {
		NewsType, Username string
	}{memberJoinNews, username})
	return r
}

func MemberQuit(username string) News {
	r, _ := json.Marshal(struct {
		NewsType, Username string
	}{memberQuitNews, username})
	return r
}
