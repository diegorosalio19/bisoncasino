package games

type Suit string

const (
	Spades   = Suit("spades")
	Hearts   = Suit("hearts")
	Diamonds = Suit("diamonds")
	Clubs    = Suit("clubs")
)

type Card struct {
	Number int
	Suit   Suit
}

type CombinationMajor string

func (r CombinationMajor) Compare(b CombinationMajor) int {
	cmValue := map[CombinationMajor]int{
		HighCard:      0,
		Pair:          1,
		TwoPair:       2,
		ThreeOfAKind:  3,
		Straight:      4,
		Flush:         5,
		FullHouse:     6,
		FourOfAKind:   7,
		StraightFlush: 8,
	}
	av, ok := cmValue[r]
	if !ok {
		av = -1
	}
	bv, ok := cmValue[b]
	if !ok {
		bv = -1
	}
	switch {
	case av > bv:
		return 1
	case av == bv:
		return 0
	case av < bv:
		return -1
	}
	return 0
}

const (
	HighCard      = CombinationMajor("HighCard")
	Pair          = CombinationMajor("Pair")
	TwoPair       = CombinationMajor("TwoPair")
	ThreeOfAKind  = CombinationMajor("ThreeOfAKind")
	Straight      = CombinationMajor("Straight")
	Flush         = CombinationMajor("Flush")
	FullHouse     = CombinationMajor("FullHouse")
	FourOfAKind   = CombinationMajor("FourOfAKind")
	StraightFlush = CombinationMajor("StraightFlush")
)

type Combination struct {
	CombinationMajor CombinationMajor
	Score            int
}

// Compare -1: a<b, 0: a==b, 1: a>b
func (a Combination) Compare(b Combination) int {
	r := a.CombinationMajor.Compare(b.CombinationMajor)
	if r != 0 {
		return r
	}
	switch {
	case a.Score > b.Score:
		return 1
	case a.Score == b.Score:
		return 0
	case a.Score < b.Score:
		return -1
	}
	return 0
}
