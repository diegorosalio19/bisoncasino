package games

type PlayPokerSlotRequest struct {
	Bet float32
}

type PlayPokerSlotResponse struct {
	Cards       []Card
	Combination string
	Value       float32
}
