package poker

import (
	"bisoncasino.net/api/games"
	"bisoncasino.net/api/news"
	"encoding/json"
)

const (
	playerPlayed         = "PlayerPlayed"
	potChanged           = "PotChanged"
	privateCardsObtained = "PrivateCardsObtained"
	publicCardsObtained  = "PublicCardsObtained"
	outcome              = "Outcome"
	playersList          = "PlayersList"
	playerBalanceChanged = "PlayerBalanceChanged"
	play                 = "Play"
	playerPlaying        = "PlayerPlaying"
	gameStarted          = "GameStarted"
	gameEnded            = "GameEnded"
	cardsShowed          = "CardsShowed"
)

type PlayerAction string

const (
	Fold   = PlayerAction("fold")
	Call   = PlayerAction("call")
	Check  = PlayerAction("check")
	Recall = PlayerAction("recall")
)

func PlayerPlayed(username string, action PlayerAction, totalBet int) news.News {
	r, _ := json.Marshal(struct {
		NewsType, Username string
		Action             PlayerAction
		TotalBet           int
	}{
		NewsType: playerPlayed,
		Username: username,
		Action:   action,
		TotalBet: totalBet,
	})
	return r
}

func PotChanged(value int) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
		Value    int
	}{potChanged, value})
	return r
}

func CardsObtained(cards []games.Card, public bool) news.News {
	nt := publicCardsObtained
	if !public {
		nt = privateCardsObtained
	}
	r, _ := json.Marshal(struct {
		NewsType string
		Cards    []games.Card
	}{nt, cards})
	return r
}

func CardsShowed(m map[string][]games.Card) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
		Map      map[string][]games.Card
	}{cardsShowed, m})
	return r
}

func Outcome(combination games.Combination, winners map[string]int) news.News {
	r, _ := json.Marshal(struct {
		NewsType    string
		Combination games.Combination
		Winners     map[string]int
	}{outcome, combination, winners})
	return r
}

type Player struct {
	Username string
	Balance  int
}

func PlayerList(players []Player) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
		Players  []Player
	}{playersList, players})
	return r
}

func PlayerBalanceChanged(username string, value int) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
		Username string
		Value    int
	}{playerBalanceChanged, username, value})
	return r
}

func PlayRequest(minBet, lastBet int) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
		MinBet   int
		LastBet  int
	}{play, minBet, lastBet})
	return r
}

func PlayResponse(n news.News) struct {
	PlayerAction PlayerAction
	RecallValue  int
} {
	r := struct {
		PlayerAction PlayerAction
		RecallValue  int
	}{}
	_ = json.Unmarshal(n, &r)
	return r
}

func PlayerPlaying(username string) news.News {
	return _username(playerPlaying, username)
}

func GameStarted() news.News {
	return _void(gameStarted)
}

func GameEnded() news.News {
	return _void(gameEnded)
}

func _void(nt string) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
	}{nt})
	return r
}

func _username(nt, username string) news.News {
	r, _ := json.Marshal(struct {
		NewsType string
		Username string
	}{nt, username})
	return r
}
