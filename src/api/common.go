package api

import (
	"net/http"
	"strings"
)

func ParseField(str string, prefix string) []string {
	var fields = strings.SplitAfter(str, prefix)
	if len(fields) == 2 {
		fields = strings.Split(fields[1], "/")
		for i := 0; i < len(fields); i++ {
			if fields[i] == "" {
				fields = append(fields[:i], fields[i+1:]...)
				i -= 1
			}
		}
	} else {
		fields = make([]string, 0)
	}
	return fields
}

func GetToken(request *http.Request) string {
	return request.Header.Get("X-Token")
}

func InitJSONPublicResponse(writer http.ResponseWriter) {
	writer.Header().Set("Access-Control-Allow-Origin", "*")
	writer.Header().Set("Access-Control-Allow-Methods", "*")
	writer.Header().Set("Access-Control-Allow-Headers", "*")
	writer.Header().Set("Content-Type", "application/json")
}
